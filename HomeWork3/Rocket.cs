﻿
namespace HomeWork3
{
    public class Rocket
    {
        public string Name;
        private int speed;

        public Rocket(string name, int speed)
        {
            Name = name;
            Speed = speed;
        }

        Rocket[] data;

        public int Speed { get => speed; set => speed = value; }

        public Rocket()
        {
            data = new Rocket[5];
        }

        public Rocket this[int index]
        {
            get
            {
                return data[index];
            }
            set
            {
                data[index] = value;
            }
        }

        public static Rocket operator +(Rocket a, Rocket b)
        {
            return new Rocket(a?.Name, a.Speed + b.Speed);
        }

        public override string ToString()
        {
            return "Ракета: " + Name + " Скорость: " + Speed;
        }
    }
}
