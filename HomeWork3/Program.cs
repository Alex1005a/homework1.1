﻿using System;

namespace HomeWork3
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Скажите название ракеты: ");
            string name = Console.ReadLine();
            Console.Write("Скажите скорость ракеты: ");
            int speed = Convert.ToInt32(Console.ReadLine());

            Rocket rocket = new Rocket();
            rocket[0] = new Rocket(name, speed);
            rocket[1] = new Rocket("Союз-1", 30000);

            Rocket UserRocket = rocket[0];
            Console.WriteLine(UserRocket);
            Console.WriteLine(UserRocket.Сalculation());

            Rocket rocket1 = rocket[1];

            Console.WriteLine($"Можете модифицировать свою ракету с помощью {rocket1}");
            Console.Write("Напишите да, если хотите и нет если не хотите: ");
            string answer = Console.ReadLine();


            if (answer == "да")
            {
                Rocket c = UserRocket + rocket1;
                Console.WriteLine(c.Сalculation());
            }

            if (answer == "нет")
            {
                Console.WriteLine("Хорошо");
            }

            if (answer != "да" && answer != "нет")
            {
                Console.WriteLine("Вы ввели что-то не то");
            }

            Console.ReadKey();
        }
    }


    public static class StringExtension
    {
        public static string Сalculation(this Rocket rocket)
        {
            if (rocket?.Speed >= 40000)
            {
                return $"Ракета {rocket.Name} долетела до Луны";
            }

            if (rocket?.Speed < 40000 && rocket?.Speed >= 29000)
            {
                return $"Ракета {rocket.Name} попала на около земную арбиту Земли";
            }

            else
            {
                return $"Ракета {rocket.Name} не взлетела";
            }
        }
    }
}
